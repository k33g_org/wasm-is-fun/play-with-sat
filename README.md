# play-with-sat

## Add a new rust function: `HelloWorld`

### Prerequisites

- You need to install the `subo` cli (👀 see: https://github.com/suborbital/subo)
- You need to install **Sat** (👀 see: https://github.com/suborbital/sat)

### Create the function skeleton

```bash
subo create runnable hello-world --dir ./modules --lang rust
````

If needed, add your dependencies to the `Cargo.toml` file. Like below (I need `serde` dependency to parse JSON string):

```toml
[dependencies]
suborbital = '0.12.0'
serde = { version = "1.0", features = ["derive"] }
serde_json = "1.0"
```

## Build the function

```bash
subo build ./modules/hello-world
```

## Use the function with Sat

### Run Sat

```bash
SAT_HTTP_PORT=8080 sat ./modules/hello-world/hello-world.wasm 
```

Now, **Sat** is serving the function on `localhost:8080`

### Call the function with curl

```bash
data='{"text":"from Bob Morane"}'
curl -d "${data}" \
    -H "Content-Type: application/json" \
    -X POST "http://localhost:8080"
```

or with Httpie:

```bash
http POST "http://localhost:8080" \
     text="from Bob Morane" 
```
