```
subo create runnable hello-people --dir ./modules --lang swift

subo build ./modules/hello-people

# PATH=$PATH:~/.sat
SAT_HTTP_PORT=8080 sat ./modules/hello-people/hello-people.wasm 

data="Bob Morane"
curl -d "${data}" \
    -H "Content-Type: application/json" \
    -H "message: 'ping'" \
    -X POST "http://localhost:8080"

echo 'Bob Morane' | http POST "http://localhost:8080" message:"pong"
```

