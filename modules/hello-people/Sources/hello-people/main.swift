import Suborbital

class HelloPeople: Suborbital.Runnable {
    func run(input: String) -> String {
        
        return "hello " + input + " " + ReqHeader(key:"message")
    }
}

Suborbital.Set(runnable: HelloPeople())
