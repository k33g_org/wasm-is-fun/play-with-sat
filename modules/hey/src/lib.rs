use suborbital::runnable::*;
use suborbital::req;

struct Hey{}

impl Runnable for Hey {
    fn run(&self, input: Vec<u8>) -> Result<Vec<u8>, RunErr> {
        let in_string = String::from_utf8(input).unwrap();
        let header_message = req::header(&"message");
    
        Ok(String::from(format!("hello {} message: {}", in_string, header_message)).as_bytes().to_vec())
    }
}


// initialize the runner, do not edit below //
static RUNNABLE: &Hey = &Hey{};

#[no_mangle]
pub extern fn _start() {
    use_runnable(RUNNABLE);
}
